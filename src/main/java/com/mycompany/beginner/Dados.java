/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.beginner;

/**
 *
 * @author ppagotto
 */
public class Dados {
    
	public String dados1;
	public String cabecalho1;
	
	public String dados2;
	public String cabecalho2;

	public String dados3;
	public String cabecalho3;
	
	public String vantagens;
	public String desvantagens;
	public String exemplos;
	
	public String getDados1() {
		return dados1;
	}

	public void setDados1(String dados1) {
		this.dados1 = dados1;
	}

	public String getCabecalho1() {
		return cabecalho1;
	}

	public void setCabecalho1(String cabecalho1) {
		this.cabecalho1 = cabecalho1;
	}

	public String getDados2() {
		return dados2;
	}

	public void setDados2(String dados2) {
		this.dados2 = dados2;
	}

	public String getCabecalho2() {
		return cabecalho2;
	}

	public void setCabecalho2(String cabecalho2) {
		this.cabecalho2 = cabecalho2;
	}

	public String getDados3() {
		return dados3;
	}

	public void setDados3(String dados3) {
		this.dados3 = dados3;
	}

	public String getCabecalho3() {
		return cabecalho3;
	}

	public void setCabecalho3(String cabecalho3) {
		this.cabecalho3 = cabecalho3;
	}

	public String getVantagens() {
		return vantagens;
	}

	public void setVantagens(String vantagens) {
		this.vantagens = vantagens;
	}

	public String getDesvantagens() {
		return desvantagens;
	}

	public void setDesvantagens(String desvantagens) {
		this.desvantagens = desvantagens;
	}

	public String getExemplos() {
		return exemplos;
	}

	public void setExemplos(String exemplos) {
		this.exemplos = exemplos;
	}



	public Dados getDados(){
		Dados poo = new Dados();
		
                /*Poo X Estruturado*/
		poo.setDados1("Para iniciarmos a comparação das duas programações, podemos citar a diferença da estrutura de cada uma."
					+"O desenvolvimento da programação estruturada é realizado através de procedimentos , aplicados em dados globais."
					+"Já o desenvolvimento da programação orientada a objetos são separados por dados do objeto e cada objeto tem seu método. "
					+"A linguagem c, é uma das principais linguagens da estruturada, ela é de baixo nível e muito utilizada para programação de sistemas embarcados e hardware."
					+"Quando a programação estruturada é bem feita, tem um desempenho melhor que a orientada a objetos. Pelo fato de ser um paradigma sequencial, em que cada linha de código"
					+"é executada após a outra, sem muitos desvios, como vemos na POO. Também permite mais liberdade de hardware, que auxilia no desempenho."
					+"Para aplicações modernas a programação orientada a objetos é mais interessante." 
					+"Ela é muito difundida, pois permite a reutilização do código e por representar o sistema parecido com o mundo real.");
		/*4 pilares*/
                
                poo.setDados2(
"Abstração: um dos pontos mais importantes em qualquer linguagem orientada a objetos. "+
"Como estamos lidando com uma representação de um objeto real (o que dá nome ao paradigma), "+
"temos que imaginar o que esse objeto irá realizar dentro de nosso sistema. Três pontos tem"+
"que ser levados em consideração:"+
"- Identidade : O objeto que vamos criar tem que ter uma identidade."+
"- Propriedade: É a característica do objeto."+
"- Métodos: São as ações que o objeto irá executar."+
"              "+
"Encapsulamento: Uma das principais técnicas que define a programação orientada a objetos."
        + " É um dos elementos que adicionam segurança à aplicação em uma POO, "
        + "porque ele esconde as propriedades, criando uma espécie de caixa preta."+
"Maioria das linguagens orientadas a objetos, utilizam o encapsulamento baseado em propriedades privadas,"
        + " ligadas a métodos especiais chamados getters e setters, que retorna e seta o valor da propriedade,"
        + " respectivamente. Com isso, evita o acesso direto a propriedade do objeto, adicionando uma outra"
        + " camada de segurança à aplicação."+
"    "+
"Herança: Uma das principais característica para o reuso de código. Ela otimiza a produção da aplicação em tempo"
                + " e linhas de código."+
"O  objeto que estiver abaixo na hierarquia irá herdar características de todos "
+ "os objetos acima dele, seus “ancestrais”. A herança a partir das características"
+ " do objeto mais acima é considerada herança direta, enquanto as demais são "
+ "consideradas heranças indiretas."+
"A herança varia de linguagem para linguagem. Em C++, há a questão da herança múltipla."
+ " Significa que o objeto pode herdar características de vários “ancestrais” ao mesmo "
+ "tempo diretamente. Devido a problemas, essa prática não foi difundida em linguagens "
+ "mais modernas, que utilizam outras artimanhas para criar uma espécie de herança múltipla."
+"Em C#, trazem um objeto base para todos os demais. A classe object fornece características"
+ " para todos os objetos, sejam criados pelo usuário ou não."+
"    "+
"Polimorfismo: Altera a forma conforme a necessidade."+
"Os objetos filhos herdam as características e ações de seus “ancestrais”. "
+ "Entretanto, em alguns casos, é necessário que as ações "
+ "para um mesmo método seja diferente. O polimorfismo consiste na alteração "
+ "do funcionamento interno de um método herdado de um objeto pai."+
"Como o polimorfismo está intimamente conectado à herança, "
+ "precisamos entender os dois juntamente. "+
"As linguagens, implementam o polimorfismo de maneiras diferentes. "
+ "O C#, por exemplo, faz uso de método virtuais (com a palavra-chave virtual)"
+ " que podem ser reimplementados (com a palavra-chave override) nas classes filhas. "
+ "Já em Java, apenas o atributo “@Override” é necessário.");
                
                /*Vantagens*/
                poo.setVantagens(
"Reutilização do código: devido aos elementos serem independentes."
+"Leitura e manutenção do código: pelo sistema se aproximar do que vemos no mundo real,"
                + " o entendimento dele, fica mais simples. Isso também ajuda a equipe de desenvolvimento "
                + "não depender só de uma pessoa, como aconteceria em linguagens estruturadas."+
"Criação bibliotecas: é mais simples com orientação a objetos. em estruturadas temos bibliotecas"
+ " que são coleções de procedimentos e permite que seja reutilizada. Em POO, elas trazem representações "
+ "de classes que são mais claras para reutilização.");
                
                /*Desvantangens*/
                poo.setDesvantagens(
                        "Lentidão: aplicação orientada a objetos é mais lenta do que a estruturada,"
                                + " porque as representações têm muitos desvios, já na estruturada a execução"
                                + " é sequencial. Por isso a preferência pela linguagem C em hardware limitado. "
                                + "E Android seja feito menos orientado a objetos."

                );
                
                /*exemplos de linguagens*/
                
                poo.setExemplos(
"Java: linguagem mais utilizada do mercado atual. "
+"Ele implementa os 4 pilares de forma intuitiva, facilitando o entendimento do desenvolvedor. "
+"A abstração é implementada através de classes que contém propriedades e métodos. "
+"O encapsulamento é realizado através de propriedades privadas, auxiliadas por "+
"getters e setters. Herança e polimorfismo, em Java são mais  complexas." +
 "Ele possui herança simples, assim cada classe pode herdar apenas de uma "
   + "outra. essas interfaces precisam ser implementadas para funcionar, significa que"
   + " você tem que implementar várias interfaces e herdar apenas uma classe. Em polimorfismo o "
   + "atributo @Override informa ao Java o método que está sendo reescrito."
                +" "
 +"C#: Uma das linguagens mais utilizadas. Como a maioria dos computadores possuem "
                + "sistema operacional Windows, o C# se popularizou. Porque o C# está associado ao "
                + "Framework .NET que Windows implementa. Ele é uma linguagem de uso geral, especialmente "
                + "criada para POO. Em C# tudo é um objeto. "+
"Na utilização dos 4 pilares, em abstração ele segue o modelo de Java."
+ "  No encapsulamento a variável que guarda o dado é chamada de atributo e a"
+ " propriedade é o elemento que acessa o dado do mundo externo. C# também faz uso de "
+ "duas palavras especiais: get e set."+
"Herança, também segue o modelo de Java. O polimorfismo, é baseado em métodos virtuais, "
+ "com virtual na classe pai e reescritos com override na classe filha.)");
                
		return poo; 
	}
	
   
    
}
